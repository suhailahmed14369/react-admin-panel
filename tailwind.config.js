module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'class',
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        mainBackground : "radial-gradient(circle, #b1e5f2, #c1e5f7, #d1e6f8, #dee7f6, #e8e9f3)",
        darkImage: "radial-gradient(circle, #12112a, #181836, #1e1e42, #24254f, #2b2c5c)",
        baseGradient : 'linear-gradient(to bottom, #e8e9f3, #eaebf4, #ecedf5, #eeeff5, #f0f1f6)'
      },
      colors : {
        'darkColor' : '#173e4e',
        gray : {
          '100' : '#666981',
          '200' : '#cfcfd3',
          'light' : '#CECECE',
          'ghost' : '#E8E9F3',
          'silver' : '#A6A6A8',
          'raisin' : '#272635',
          'ghost-100' : '#f1f1f7'
        },
        blue : {
          photo : '#B1E5F2',
          'photo-100' : '#80ddf4',
        }
      },
      blur: {
        xs: '3px',
      },
      dropShadow: {
        'custom': '0px 4px 4px rgba(0, 0, 0, 0.25)',
      },
      height : {
        mainBox : 'calc(100% - 64px)'
      },
      width : {
        '3.375' : '3.375rem',
        '60px' : '60px',
      },
      maxWidth: {
        '70%' : '70%',
        '80%': '80%',
      },
      minWidth: {
        '20%': '20%',
      }
    },
  },
  plugins: [
    require('@tailwindcss/forms')
  ],
}
