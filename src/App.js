import './App.css';
import Layout from './Component/Layout/Layout.jsx';
import Login from './Component/Login/Login';
import useToken from './useToken';
import {TokenProvider} from './Component/TokenContext';

function App() {
  const { token, setToken } =  useToken();

  if(!token){
    return(
     <TokenProvider> <Login setToken={setToken}/></TokenProvider>
    )
    }

  return (
    <TokenProvider>    
      <div className="App h-screen max-h-screen">
      <Layout/>
    </div></TokenProvider>
  );
}

export default App;
