import { Fragment, useState } from 'react'

import { Dialog, Menu, Transition } from '@headlessui/react'
import {
  BellIcon,
  CollectionIcon,
  MenuAlt2Icon,
  XIcon,
  ChatIcon
} from '@heroicons/react/outline'
import { SearchIcon } from '@heroicons/react/solid'
import { MoonIcon } from '../../Svg/MoonIcon'
import { SunIcon } from '../../Svg/SunIcon'
import { blue } from 'tailwindcss/colors'
import Dashboard from '../Dashboard/Dashboard'
import Chat from '../Chat/Chat'
import { Routes, Route , Link } from 'react-router-dom'
import NavLink from '../NavLink'

const navigation = [
  { name: 'Dashboard', to: '/', icon: CollectionIcon, current: false,  },
  { name: 'Chat', to: '/chat', icon: ChatIcon, current: false },
]


function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function Layout() {
  const [sidebarOpen, setSidebarOpen] = useState(true)

  const [icon, setIcon] = useState(true);

  const handleToggleTheme = () => {
      setIcon(!icon);
      const htmlTag = document.getElementsByTagName('html')[0].classList.toggle('dark');
  }

  const mode = document.getElementsByTagName('html')[0].classList.contains('dark') ? 'white' : 'black'


  const logout = () => {
    localStorage.clear();
    window.location.href = '/';
  }

  return (
    <>

  
        <Transition.Root show={sidebarOpen} as={Fragment}>
          <Dialog as="div" className="fixed inset-0 flex z-40 md:hidden" onClose={setSidebarOpen}>
            <Transition.Child
              as={Fragment}
              enter="transition-opacity ease-linear duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="transition-opacity ease-linear duration-300"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 bg-gray-600 bg-opacity-75" />
            </Transition.Child>
            <Transition.Child
              as={Fragment}
              enter="transition ease-in-out duration-300 transform"
              enterFrom="-translate-x-full"
              enterTo="translate-x-0"
              leave="transition ease-in-out duration-300 transform"
              leaveFrom="translate-x-0"
              leaveTo="-translate-x-full"
            >
              <div className="relative flex-1 flex flex-col max-w-xs w-full pt-5 pb-4 bg-gray-ghost dark:bg-darkImage bg-opacity-60 backdrop-blur-lg">
                <Transition.Child
                  as={Fragment}
                  enter="ease-in-out duration-300"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in-out duration-300"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <div className="absolute top-0 right-0 -mr-12 pt-2">
                    <button
                      type="button"
                      className="ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                      onClick={() => setSidebarOpen(false)}
                    >
                      <span className="sr-only">Close sidebar</span>
                      <XIcon className="h-6 w-6 text-white" aria-hidden="true" />
                    </button>
                  </div>
                </Transition.Child>
                <div className="flex-shrink-0 flex items-center px-4">
                  <img
                    className="h-8 w-auto"
                    src={'images/'+mode+'logo.png'}
                    alt="Workflow"
                  />
                </div>
                <div className="mt-5 flex-1 h-0 overflow-y-auto">
                  <nav className="px-2 space-y-1">
                    {navigation.map((item) => (
                     <NavLink
                     key={item.name}
                     to={item.to}
                     name={item.name}
                     icon={item.icon}
                     className='text-gray-raisin dark:text-gray-ghost hover:bg-gray-ghost group flex items-center px-2 py-2 text-sm font-medium rounded-md'
                   > 
                   </NavLink>
                    ))}
                  </nav>
                </div>
              </div>
            </Transition.Child>
            <div className="flex-shrink-0 w-14" aria-hidden="true">
              {/* Dummy element to force sidebar to shrink to fit close icon */}
            </div>
          </Dialog>
        </Transition.Root>

        {/* Static sidebar for desktop */}
        <div className="hidden md:flex md:w-48 md:flex-col md:fixed md:inset-y-0">
          {/* Sidebar component, swap this element with another sidebar if you like */}
          <div className="flex flex-col flex-grow pt-5  overflow-y-auto">
            <div className="flex items-center flex-shrink-0 px-4">
            {/* <picture>
  <source 
    srcset="images/whitelogo.png" 
    media="(prefers-color-scheme: dark)" />
  <img src="images/bluelogo.png" />
</picture> */}
              <img
                className="h-7 w-auto mx-auto"
                src={'images/'+mode+'logo.png'}
                alt="Workflow"
              />
            </div>
            <div className="mt-5 flex-1 flex flex-col">
              <nav className="flex-1 px-2 pb-4 space-y-1">
                {navigation.map((item) => (
                  <NavLink
                    key={item.name}
                    to={item.to}
                    name={item.name}
                    icon={item.icon}
                    className='text-gray-raisin dark:text-gray-ghost hover:bg-gray-ghost group flex items-center px-2 py-2 text-sm font-medium rounded-md'
                  > 
                  </NavLink>
                ))}
              </nav>
            </div>
          </div>
        </div>
        <div className="md:pl-64 flex flex-col flex-1 h-full">
          <div className="sticky top-0 z-10 flex-shrink-0 flex h-16 ">
            <button
              type="button"
              className="px-4 border-r border-gray-200 text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500 md:hidden"
              onClick={() => setSidebarOpen(true)}
            >
              <span className="sr-only">Open sidebar</span>
              <MenuAlt2Icon className="h-6 w-6" aria-hidden="true" />
            </button>
            <div className="flex-1 px-4 flex justify-between">
              <div className="flex-1 flex">
              {/* <button
                      type="button"
                      className="ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                      onClick={() => setSidebarOpen(false)}
                    >
                      <span className="sr-only">Close sidebar</span>
                      <XIcon className="h-6 w-6 text-white" aria-hidden="true" />
                    </button> */}
                <form className="w-full flex md:ml-0 pl-4" action="#" method="GET">
                  <label htmlFor="search-field" className="sr-only">
                    Search
                  </label>
                  <div className="relative w-full text-gray-400 focus-within:text-gray-600">
                    <div className="absolute inset-y-0 left-0 flex items-center pointer-events-none">
                      <SearchIcon className="h-5 w-5" aria-hidden="true" />
                    </div>
                    <input
                      id="search-field"
                      className="block bg-transparent w-full h-full pl-8 pr-3 py-2 border-transparent text-gray-900 placeholder-gray-500 focus:outline-none focus:placeholder-gray-400 focus:ring-0 focus:border-transparent sm:text-sm"
                      placeholder="Search"
                      type="search"
                      name="search"
                    />
                  </div>
                </form>
              </div>
              <div className="ml-4 flex items-center md:ml-6">
                <button
                  type="button"
                  className="bg-white p-1 rounded-full text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                >
                  <span className="sr-only">View notifications</span>
                  <BellIcon className="h-6 w-6" aria-hidden="true" />
                </button>

                {/* Profile dropdown */}
                <Menu as="div" className="ml-3 relative">
                  <div>
                    <Menu.Button className="max-w-xs bg-white flex items-center text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                      <span className="sr-only">Open user menu</span>
                      <img
                        className="h-8 w-8 rounded-full"
                        src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                        alt=""
                      />
                    </Menu.Button>
                  </div>
                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                  >
                    <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                     <Menu.Item>
                     <Link to='' onClick={() => logout()} className='block px-4 py-2 text-sm text-gray-700'>Sign out
                      </Link>
                     </Menu.Item>
                    </Menu.Items>
                  </Transition>
                </Menu>
                <div onClick={()=> handleToggleTheme()} className='h-8 w-8 cursor-pointer text-black ml-12'>
                {
                    icon ? <MoonIcon></MoonIcon> : <SunIcon></SunIcon>
                }
            </div>
              </div>
             
            </div>
          </div>

          <main className='h-mainBox'>
            <div className="py-10 h-full">

              <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8 h-full">
                {/* Replace with your content */}
            
             <Routes>
                <Route path='/' element={<Dashboard />} />
                <Route path='/chat' element={<Chat />} />
              </Routes>
                {/* /End replace */}
              </div>
            </div>
          </main>
        </div>
   
    </>
  )
}
