import React, { useState, useRef, useEffect, useContext } from 'react'
import PropTypes from 'prop-types';
import {TokenContext} from '../TokenContext'

async function loginUser(props) {
    return fetch("https://chatbot-api.p3pighhl5iinq.ap-south-1.cs.amazonlightsail.com/token", {
        body: "grant_type=&username="+ props.username +"&password="+ props.password +"&scope=&client_id=&client_secret=",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        method: "POST"
      }).then(data => data.json())
      
   }

const Login = ({ setToken }) => {
    const { localToken, setLocalToken} = useContext(TokenContext);
    const [username, setUserName] = useState('');
    const [password, setPassword] = useState('');

    const spanRef = useRef();
    const usernameRef = useRef();
    const spanRef2 = useRef();
    const passwordRef = useRef();


    const handleSubmit = async e => {
        e.preventDefault();
        if(username === ''){
            spanRef.current.classList.remove('hidden');
            usernameRef.current.focus();
            // usernameRef.current.classList.add('bg-red-100');
        }
        if(password === ''){
            spanRef2.current.classList.remove('hidden');
        }
        if(username != '' && password != ''){
            const token = await loginUser({
                username,
                password
              });
      
              let t = token.access_token
              console.log(t)
              setLocalToken(t)
              t = {
                  "token" : t
              }
            
              console.log(localToken)
              setToken(t);
              console.log(localToken)
        }
      }

    return (
        <div className="font-sans">
            <div className="relative min-h-screen flex flex-col sm:justify-center items-center  ">
                <div className="relative sm:max-w-sm w-full">
                    
                    <div className="relative w-full rounded-3xl  px-6 py-4 shadow-lg backdrop-blur-lg">
                        <label htmlFor="" className="block mt-3 text-sm text-gray-700 text-center font-semibold">
                            Login
                        </label>
                        <form onSubmit={handleSubmit} className="mt-10">

                            <div>
                                <input ref={usernameRef} type="text" onChange={e => setUserName(e.target.value)} placeholder="User Name" className="px-3 mt-1 block w-full border-none bg-transparent h-11 rounded-xl shadow-lg hover:bg-blue-100 focus:bg-blue-100 focus-visible:ring-0 focus:ring-0 focus-visible:outline-0" />
                                <span ref={spanRef} className="hidden text-sm pl-2 text-red-500" data-span="username">*Username is required</span>
                            </div>

                            <div className="mt-7">
                                <input ref={passwordRef} type="password" onChange={e => setPassword(e.target.value)} placeholder="Password" className="px-3 mt-1 block w-full border-none bg-transparent h-11 rounded-xl shadow-lg hover:bg-blue-100 focus:bg-blue-100 focus:ring-0 focus-visible:outline-0" />
                                <span ref={spanRef2} className="hidden text-sm pl-2 text-red-500" data-span="password">*Password is required</span>
                            </div>

                            <div className="mt-7 flex">
                                <label htmlFor="remember_me" className="inline-flex items-center w-full cursor-pointer">
                                    <input id="remember_me" type="checkbox" className="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember" />
                                        <span className="ml-2 text-sm text-gray-600">
                                            Remember Me
                                        </span>
                                </label>

                                <div className="w-full text-right">
                                    <a className="underline text-sm text-gray-600 hover:text-gray-900" href="#">
                                        Forgot Password?
                                    </a>
                                </div>
                            </div>

                            <div className="mt-7">
                                <button type='submit' className="bg-blue-500 w-full py-3 rounded-xl text-white shadow-xl hover:shadow-inner focus:outline-none transition duration-500 ease-in-out  transform hover:-translate-x hover:scale-105">
                                    Login
                                </button>
                            </div>

                        
                            <hr className="border-gray-300 border-1 w-full rounded-md" />
                                
                            
                            <div className="mt-7">
                                <div className="flex justify-center items-center">
                                    <label className="mr-2" >Need an account?</label>
                                    <a href="#" className=" text-blue-500 transition duration-500 ease-in-out  transform hover:-translate-x hover:scale-105">
                                         Create
                                    </a>
                                 </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>

            </div>
            )
}

export default Login

Login.propTypes = {
    setToken: PropTypes.func.isRequired
    }