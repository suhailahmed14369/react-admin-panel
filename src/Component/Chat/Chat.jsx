import React , {useState , useEffect, useContext} from 'react';
import Chat1 from './Chat1';
import Chat2 from './Chat2';
import ChatHistorySkeleton from './ChatHistorySkeleton';
import ChatSkeleton from './ChatSkeleton';
import {TokenContext} from '../TokenContext'

const Chat = () => {

  const [apiData , setData] = useState([]);
  const [msgId, setMsgId] = useState('All')
  const [apiLoading, setApiLoading] = useState(true)
  const [chatLoading, setChatLoading] = useState(true)
  const [chat , setChat] = useState([]);
  const tokenString = localStorage.getItem('token');
  const userToken = JSON.parse(tokenString);
  console.log(userToken?.token)


  useEffect(() => {
    const api = () => {
      fetch("https://api.icubeswire.in/chats", {
        headers: {
          Accept: "application/json",
          Authorization: "Bearer " + userToken?.token,
        }
      }).then((response) => response.json()).then((data) => {
            setData(data.items)
            data = Object.values(data.items)
            
              data = data.filter((value) => {
                  if(value.id === msgId){
                      return value

                  }else{
                      return ''
                  }
                })
              setChat(data);
              setChatLoading(false)
              setApiLoading(false)

          })
    }
    if(apiLoading){
      api()
    }
    if(chatLoading){
      api()
    }
  });
  const handleChat = (e) => {
    setMsgId(e.target.id)
    setChatLoading(true)
  }



  return (
    <>
      <div className="grid grid-cols-3 gap-4 h-full relative">
        {apiLoading ? <ChatHistorySkeleton></ChatHistorySkeleton> :
        <div className='backdrop-blur-lg bg-white rounded-lg h-full drop-shadow-custom overflow-y-auto flex flex-col'>
          <h1 className='text-lg text-gray-raisin font-bold text-left sticky top-0 bg-white shadow py-3 px-4'>Chat History</h1>
          <div className='px-2 py-2 overflow-y-auto h-full scroll-box'>
            {apiData && apiData.map((item,key) =>
              <div key={item.id} onClick={(e) =>handleChat(e)} className="flex relative py-2 items-end justify-between cursor-pointer px-2 hover:bg-blue-photo hover:bg-opacity-60 hover:rounded-md">
                <div className="dp flex items-center ">
                <img  className="h-14 w-14 rounded-full"  src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"  alt=""/>
                <div className="person-id text-gray-silver px-3">ID - {item.id}</div>
                </div>
              
                <div className="date text-sm text-gray-raisin font-semibold">{new Intl.DateTimeFormat('en-US', { hour: '2-digit', minute: '2-digit', second: '2-digit' }).format(item.updated_at)}</div>
                <div id={item.id} className="chat-overlay absolute h-full w-full"></div>
              </div>
            )}
          </div>
        </div>
      }     
        {chatLoading ? <ChatSkeleton></ChatSkeleton> :
        <div className='col-span-2 px-6 overflow-y-auto relative flex flex-col'>
            {chat && chat.map((item,key)=>
              <div key={item.id}>
                <div className="flex justify-between px-3 mb-4 items-center sticky top-0 bg-gray-ghost-100">
                  <div className="dp flex items-center ">
                    <img  className="h-14 w-14 rounded-full"  src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"  alt=""/>
                    <div className="person-id text-gray-silver px-3">ID - {item.id}</div>
                  </div>
                  <div className="date text-sm text-gray-raisin font-semibold">{item.updated_at}</div>
                </div>
    
                <div className='px-8 h-full text-left overflow-y-auto scroll-box'>
                  {chat && chat[0]['message'].map((item,index)=>
                    {
                       return(
                         <>
                          {Object.keys(item)[0] == '0000' ?
                            <div key={index} className='w-full flex justify-end'>
                              <Chat2  text={Object.values(item)[0]} time={ new Intl.DateTimeFormat('en-US', { hour: '2-digit', minute: '2-digit' }).format(Object.values(item)[1])}></Chat2>
                            </div> :
                            <div key={index} className='w-full flex'>
                              <Chat1 id={Object.keys(item)[0]} text={Object.values(item)[0]} time={ new Intl.DateTimeFormat('en-US', { hour: '2-digit', minute: '2-digit' }).format(Object.values(item)[1])}></Chat1>
                            </div>
                         }
                        
                         </>
                       )
                    }
                  )}
               
                </div>
         
              </div>
            )}
        </div> }

      </div>
    </>
  )
};

export default Chat;
