import React from 'react';

const Chat2 = (props) => {
  return (
  
        <div className="chat2 min-w-20% max-w-70% p-2 leading-4  mb-4 bg-blue-photo rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold text-white">ICUBESWIRE</div>
            <div className="flex justify-between items-end"><div className="chat">{props.text}</div>
            <div className="time text-xs font-semibold text-gray-silver w-3.375 text-right">{props.time}</div></div> 
        </div>
 
  )
};

export default Chat2;
