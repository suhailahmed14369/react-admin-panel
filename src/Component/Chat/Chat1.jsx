import React from 'react';

const Chat1 = (props) => {
  return (

        <div className="chat1 min-w-20% max-w-70% p-2 leading-4 bg-white mb-4 rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold text-blue-photo-100">ID - {props.id}</div>
            <div className="flex justify-between items-end"><div className="chat">{props.text}</div>
            <div className="time text-xs font-semibold text-gray-silver  w-3.375 text-right">{props.time}</div></div> 
        </div>
  )
};

export default Chat1;
