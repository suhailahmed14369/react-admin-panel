import React from 'react';

const ChatSkeleton = () => {
  return (
  
      
       
        <div className='col-span-2 px-6 overflow-y-auto relative flex flex-col '>
          <div className="flex justify-between px-3 mb-4 items-center sticky top-0 bg-gray-ghost-100">
            <div className="dp flex items-center animate-pulse">
                <div  className="h-14 w-14 rounded-full bg-gray-200"></div>
                <div className="person-id w-40 h-3 ml-3 bg-gray-200 px-3 rounded"></div>
            </div>
            
            <div className="date text-sm h-2 w-10 bg-gray-200 font-semibold rounded animate-pulse"></div>
          </div>
    
          <div className='px-8 h-full text-left overflow-y-auto scroll-box'>
          <div className="chat1 max-w-70% w-full p-2 leading-4 bg-white float-left mb-4 rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold w-20 h-2.5 bg-gray-200 mb-4 rounded animate-pulse"></div>
            <div className="flex items-end animate-pulse">
                <div className='w-full'>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/5 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/4 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-1/2 mr-3 rounded"></div>
                </div>
                <div className="time text-xs font-semibold bg-slate-200 h-2 w-3.375 rounded"></div>
            </div> 
          </div>

          <div className="chat2  max-w-70% w-full p-2 leading-4 float-right mb-4 bg-white rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold w-20 h-2.5 bg-gray-200 mb-4 rounded animate-pulse"></div>
            <div className="flex items-end animate-pulse">
                <div className='w-full'>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/5 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/4 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-1/2 mr-3 rounded"></div>
                </div>
                <div className="time text-xs font-semibold bg-slate-200 h-2 w-3.375 rounded"></div>
            </div> 
          </div>
 
          <div className="chat1 max-w-70% w-full p-2 leading-4 bg-white float-left mb-4 rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold w-20 h-2.5 bg-gray-200 mb-4 rounded animate-pulse"></div>
            <div className="flex items-end animate-pulse">
                <div className='w-full'>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/5 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/4 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-1/2 mr-3 rounded"></div>
                </div>
                <div className="time text-xs font-semibold bg-slate-200 h-2 w-3.375 rounded"></div>
            </div> 
          </div>

          <div className="chat2  max-w-70% w-full p-2 leading-4 float-right mb-4 bg-white rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold w-20 h-2.5 bg-gray-200 mb-4 rounded animate-pulse"></div>
            <div className="flex items-end animate-pulse">
                <div className='w-full'>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/5 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/4 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-1/2 mr-3 rounded"></div>
                </div>
                <div className="time text-xs font-semibold bg-slate-200 h-2 w-3.375 rounded"></div>
            </div> 
          </div>
 
          <div className="chat1 max-w-70% w-full p-2 leading-4 bg-white float-left mb-4 rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold w-20 h-2.5 bg-gray-200 mb-4 rounded animate-pulse"></div>
            <div className="flex items-end animate-pulse">
                <div className='w-full'>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/5 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/4 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-1/2 mr-3 rounded"></div>
                </div>
                <div className="time text-xs font-semibold bg-slate-200 h-2 w-3.375 rounded"></div>
            </div> 
          </div>

          <div className="chat2  max-w-70% w-full p-2 leading-4 float-right mb-4 bg-white rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold w-20 h-2.5 bg-gray-200 mb-4 rounded animate-pulse"></div>
            <div className="flex items-end animate-pulse">
                <div className='w-full'>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/5 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/4 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-1/2 mr-3 rounded"></div>
                </div>
                <div className="time text-xs font-semibold bg-slate-200 h-2 w-3.375 rounded"></div>
            </div> 
          </div>
 
          <div className="chat1 max-w-70% w-full p-2 leading-4 bg-white float-left mb-4 rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold w-20 h-2.5 bg-gray-200 mb-4 rounded animate-pulse"></div>
            <div className="flex items-end animate-pulse">
                <div className='w-full'>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/5 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/4 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-1/2 mr-3 rounded"></div>
                </div>
                <div className="time text-xs font-semibold bg-slate-200 h-2 w-3.375 rounded"></div>
            </div> 
          </div>

          <div className="chat2  max-w-70% w-full p-2 leading-4 float-right mb-4 bg-white rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold w-20 h-2.5 bg-gray-200 mb-4 rounded animate-pulse"></div>
            <div className="flex items-end animate-pulse">
                <div className='w-full'>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/5 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/4 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-1/2 mr-3 rounded"></div>
                </div>
                <div className="time text-xs font-semibold bg-slate-200 h-2 w-3.375 rounded"></div>
            </div> 
          </div>
 
          <div className="chat1 max-w-70% w-full p-2 leading-4 bg-white float-left mb-4 rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold w-20 h-2.5 bg-gray-200 mb-4 rounded animate-pulse"></div>
            <div className="flex items-end animate-pulse">
                <div className='w-full'>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/5 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/4 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-1/2 mr-3 rounded"></div>
                </div>
                <div className="time text-xs font-semibold bg-slate-200 h-2 w-3.375 rounded"></div>
            </div> 
          </div>

          <div className="chat2  max-w-70% w-full p-2 leading-4 float-right mb-4 bg-white rounded-md shadow text-gray-raisin"> 
            <div className="user-id text-sm font-bold w-20 h-2.5 bg-gray-200 mb-4 rounded animate-pulse"></div>
            <div className="flex items-end animate-pulse">
                <div className='w-full'>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/5 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-3/4 mr-3 mb-3 rounded"></div>
                    <div className="chat text-gray-light h-2 bg-slate-200 w-1/2 mr-3 rounded"></div>
                </div>
                <div className="time text-xs font-semibold bg-slate-200 h-2 w-3.375 rounded"></div>
            </div> 
          </div>
 
  

          </div>
         
        </div>

   
   
  )
};

export default ChatSkeleton;
