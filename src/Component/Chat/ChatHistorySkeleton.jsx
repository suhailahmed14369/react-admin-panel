import React from 'react';

const ChatHistorySkeleton = () => {
  return (
    <div className='backdrop-blur-lg bg-white rounded-lg h-full drop-shadow-custom overflow-y-auto flex flex-col'>
    <h1 className='text-lg text-gray-raisin font-bold text-left sticky top-0 bg-white shadow py-3 px-4'>Chat History</h1>
    <div className='px-2 py-2 overflow-y-auto h-full scroll-box'>
    <div className="flex py-2 items-end justify-between cursor-pointer px-2">
        <div className="dp flex items-center animate-pulse">
            <div className="h-14 w-14 rounded-full bg-slate-200"></div>
            <div className="person-id bg-slate-200 h-2.5 w-24 px-3 rounded ml-3"></div>
        </div>
        <div className="date text-sm bg-slate-200 h-2 w-12 rounded animate-pulse font-semibold"></div>
    </div>
 
    <div className="flex py-2 items-end justify-between cursor-pointer px-2">
        <div className="dp flex items-center animate-pulse">
            <div className="h-14 w-14 rounded-full bg-slate-200"></div>
            <div className="person-id bg-slate-200 h-2.5 w-24 px-3 rounded ml-3"></div>
        </div>
        <div className="date text-sm bg-slate-200 h-2 w-12 rounded animate-pulse font-semibold"></div>
    </div>
 
    <div className="flex py-2 items-end justify-between cursor-pointer px-2">
        <div className="dp flex items-center animate-pulse">
            <div className="h-14 w-14 rounded-full bg-slate-200"></div>
            <div className="person-id bg-slate-200 h-2.5 w-24 px-3 rounded ml-3"></div>
        </div>
        <div className="date text-sm bg-slate-200 h-2 w-12 rounded animate-pulse font-semibold"></div>
    </div>
 
    <div className="flex py-2 items-end justify-between cursor-pointer px-2">
        <div className="dp flex items-center animate-pulse">
            <div className="h-14 w-14 rounded-full bg-slate-200"></div>
            <div className="person-id bg-slate-200 h-2.5 w-24 px-3 rounded ml-3"></div>
        </div>
        <div className="date text-sm bg-slate-200 h-2 w-12 rounded animate-pulse font-semibold"></div>
    </div>
 
    <div className="flex py-2 items-end justify-between cursor-pointer px-2">
        <div className="dp flex items-center animate-pulse">
            <div className="h-14 w-14 rounded-full bg-slate-200"></div>
            <div className="person-id bg-slate-200 h-2.5 w-24 px-3 rounded ml-3"></div>
        </div>
        <div className="date text-sm bg-slate-200 h-2 w-12 rounded animate-pulse font-semibold"></div>
    </div>
 
    <div className="flex py-2 items-end justify-between cursor-pointer px-2">
        <div className="dp flex items-center animate-pulse">
            <div className="h-14 w-14 rounded-full bg-slate-200"></div>
            <div className="person-id bg-slate-200 h-2.5 w-24 px-3 rounded ml-3"></div>
        </div>
        <div className="date text-sm bg-slate-200 h-2 w-12 rounded animate-pulse font-semibold"></div>
    </div>
 
    <div className="flex py-2 items-end justify-between cursor-pointer px-2">
        <div className="dp flex items-center animate-pulse">
            <div className="h-14 w-14 rounded-full bg-slate-200"></div>
            <div className="person-id bg-slate-200 h-2.5 w-24 px-3 rounded ml-3"></div>
        </div>
        <div className="date text-sm bg-slate-200 h-2 w-12 rounded animate-pulse font-semibold"></div>
    </div>
 
    <div className="flex py-2 items-end justify-between cursor-pointer px-2">
        <div className="dp flex items-center animate-pulse">
            <div className="h-14 w-14 rounded-full bg-slate-200"></div>
            <div className="person-id bg-slate-200 h-2.5 w-24 px-3 rounded ml-3"></div>
        </div>
        <div className="date text-sm bg-slate-200 h-2 w-12 rounded animate-pulse font-semibold"></div>
    </div>
 

    </div>
  </div>
  )
};

export default ChatHistorySkeleton;
