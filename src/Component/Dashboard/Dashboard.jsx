import React from 'react';

const Dashboard = () => {
  return (
      <div className=' backdrop-blur-lg bg-baseGradient rounded-lg h-full w-full drop-shadow-custom'></div>
  )
};

export default Dashboard;
