import React , {useState, createContext} from 'react';

export const TokenContext = createContext();

export const TokenProvider = (props) => {
    const [localToken, setLocalToken] = useState();
      const value = { localToken, setLocalToken}
    return(
        <TokenContext.Provider value={value}>
            {props.children}
        </TokenContext.Provider>
    )
}