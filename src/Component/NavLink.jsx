import React from 'react';
import { Link, useLocation } from 'react-router-dom';

const NavLink = (props) => {
    var isActive = useLocation().pathname === props.to ? ' active' : '';
  return (
    <Link className={props.className + isActive} to={props.to}>
      <props.icon className="mr-3 flex-shrink-0 h-6 w-6 text-gray-raisin dark:text-gray-ghost" aria-hidden="true" />
      {props.name}
    </Link>
  )
};

export default NavLink;
